<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('clients', 'api\v1\ClientController');
Route::get('/cars/rented', ['as' => 'cars.rented', 'uses' => 'api\v1\CarController@getRentedCars']);
Route::get('/cars/free', ['as' => 'cars.free', 'uses' => 'api\v1\CarController@getAvailableCars']);
Route::resource('cars', 'api\v1\CarController');
Route::resource('rentals', 'api\v1\RentalController');
Route::get('/histories/client/{id}', ['as' => 'histories.client', 'uses' => 'api\v1\ClientController@getRentHistories']);
Route::get('/histories/car/{id}', ['as' => 'histories.car', 'uses' => 'api\v1\CarController@getRentHistories']);
