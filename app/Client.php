<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends BaseModel
{
  public $timestamps  = false;
  protected $fillable = array('name', 'gender');
  protected $rules    = array(
    'name'    => 'required',
    'gender'  => 'required|in:male,female'
    );

  public function histories()
  {
    return $this->belongsToMany('App\Car', 'rentals')
    ->join('cars as c', 'rentals.car_id', '=', 'c.id')
    ->select(['c.brand', 'c.type', 'c.plate', 'rentals.date_from', 'rentals.date_to']);
    // ->select(['date_from', 'date_to']);
  }
}